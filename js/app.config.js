(function(){

	'use strict';

	angular
		.module('app.module', ['ui.router'])
		.config(function($stateProvider, $urlRouterProvider){
				$stateProvider
					.state('calc_app', {
						url: '/',
						templateUrl:'calc_app.html',
						controller: 'CalcCtrl',
						controllerAs: 'cc',
					});
			})
})();