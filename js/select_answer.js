(function() {
    'use strict';

    angular
        .module('calc_app.module')
        .directive('selectAnswer', selectAnswer);

    selectAnswer.$inject = ['$compile'];

    function selectAnswer($compile) {
        var directive = {
            templateUrl: '',
            restrict: 'EA',
            controller: 'CalcCtrl',
            controllerAs: 'cc',
            scope: {
                answer: "=",
            },
            link: function(scope, element) {
                element.on("click", function() {
                    $('#step_' + scope.answer).removeClass('active_p').addClass('done_p');
                    $('#step_' + (scope.answer + 1)).attr('select-step', "");
                    $('#step_' + (scope.answer + 1)).attr('step', "" + (scope.answer + 1) + "");
                    if (!($('#step_' + (scope.answer + 1)).hasClass("done_p")))
                        $('#step_' + (scope.answer + 1)).addClass('active_p');
                    $('#step_' + (scope.answer + 1)).addClass('animated bounce');
                    setTimeout(function() { $('#step_' + (scope.answer + 1)).removeClass('animated bounce'); }, 1000);

                    $compile('#step_' + (scope.answer + 1))(scope);

                    $('#question_' + (scope.answer)).addClass('animated fadeOut');
                    setTimeout(function() {
                        $('#question_' + (scope.answer)).removeClass('animated fadeOut');
                        $('#question_' + (scope.answer + 1)).addClass('animated fadeIn');
                        $compile('#question_' + (scope.answer));
                        for (var i = 1; i < 11; i++) {
                            $('#question_' + i)[0].style.display = 'none';
                        }
                        if (scope.answer != 10) {
                            $('#question_' + (scope.answer + 1))[0].style.display = 'block';
                        }
                    }, 1000);
                });
            }
        };

        return directive;
    };
})();
