(function() {
    'use strict';

    angular
        .module('calc_app.module')
        .directive('selectStep', selectStep);

    function selectStep() {
        var directive = {
            templateUrl: '',
            restrict: 'EA',
            controller: 'CalcCtrl',
            controllerAs: 'cc',
            scope: {
                step: "=",
            },
            link: function(scope, element) {
                element.on("click", function() {
                    $('#step_' + scope.step).addClass('active_p');
                    $('#step_' + (scope.step)).addClass('animated bounce');
                    $('#question_' + (scope.step)).addClass('animated fadeIn');
                    setTimeout(function() { $('#step_' + (scope.step)).removeClass('animated bounce'); }, 1000);
                    for (var i = 1; i < 11; i++) {
                        $('#question_' + i)[0].style.display = 'none';
                    }
                    $('#question_' + scope.step)[0].style.display = 'block';
                });
            }
        };

        return directive;
    };
})();
