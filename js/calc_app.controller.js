(function() {
    'use strict';

    angular
        .module('calc_app.module')
        .controller('CalcCtrl', CalcCtrl);

    CalcCtrl.$inject = ['$timeout'];

    function CalcCtrl($timeout) {
        var vm = this;
        vm.q1 = { selection: 0, price: 0, description: "" };
        vm.q2 = { selection: 0, price: 0, description: "" };
        vm.q3 = { selection: 0, price: 0, description: "" };
        vm.q4 = { selection: 0, price: 0, description: "" };
        vm.q5 = { selection: 0, price: 0, description: "" };
        vm.q6 = { selection: 0, price: 0, description: "" };
        vm.q7 = { selection: 0, price: 0, description: "" };
        vm.q8 = { selection: 0, price: 0, description: "" };
        vm.q9 = { selection: 0, price: 0, description: "" };
        vm.q10 = { selection: 0, price: 0, description: "" };
        vm.total = 0;

        //functions
        vm.question1 = question1;
        vm.question2 = question2;
        vm.question3 = question3;
        vm.question4 = question4;
        vm.question5 = question5;
        vm.question6 = question6;
        vm.question7 = question7;
        vm.question8 = question8;
        vm.question9 = question9;
        vm.question10 = question10;
        vm.finish = finish;
        vm.send_mail = send_mail;

        $(document).ready(function() {
            $('body').removeClass("load").addClass("loaded");
        });

        var SPMaskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000 - 0000' : '(00) 0000 - 00009';
        };

        var spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

        $('#phone').mask(SPMaskBehavior, spOptions);

        function question1(selection, price) {
            vm.q1.selection = selection;
            vm.q1.price = price;

            vm.finish();
        };

        function question2(selection, price) {
            vm.q2.selection = selection;
            vm.q2.price = price;

            vm.finish();
        }

        function question3(selection, price) {
            vm.q3.selection = selection;
            vm.q3.price = price;

            vm.finish();
        };

        function question4(selection, price) {
            vm.q4.selection = selection;
            vm.q4.price = price;

            vm.finish();
        };

        function question5(selection, price) {
            vm.q5.selection = selection;
            vm.q5.price = price;

            vm.finish();
        };

        function question6(selection, price) {
            vm.q6.selection = selection;
            vm.q6.price = price;

            vm.finish();
        };

        function question7(selection, price) {
            vm.q7.selection = selection;
            vm.q7.price = price;

            vm.finish();
        };

        function question8(selection, price) {
            vm.q8.selection = selection;
            vm.q8.price = price;

            vm.finish();
        };

        function question9(selection, price) {
            vm.q9.selection = selection;
            vm.q9.price = price;

            vm.finish();
        };

        function question10(selection, price) {
            vm.q10.selection = selection;
            vm.q10.price = price;

            vm.finish();
        };

        function finish() {
            vm.total = vm.q1.price + vm.q2.price + vm.q3.price + vm.q4.price + vm.q5.price + vm.q6.price + vm.q7.price + vm.q8.price + vm.q9.price + vm.q10.price;
            setTimeout(function() {
                Translate.init((window.location.hash) ? window.location.hash : "#br");
            });
            if (vm.q10.selection != 0) {
                setTimeout(function() {
                    $('#final')[0].style.display = 'block';
                    for (var i = 1; i < 11; i++) {
                        $('#question_' + i)[0].style.display = 'none';
                    }
                }, 1000);
            }
        };

        // Função para enviar e-mail com todos os dados
        function send_mail() {
            // Descomentar corpo da função para enviar o e-mail

            //     e.preventDefault(); //prevent form from submitting
                var data = {
                    name: $("#name").val(),
                    phone: $("#phone").val(),
                    email: $("#email").val(),
                    message: $('#table')[0].innerText + "\n\n" + $("#message").val()
                };
                console.log(data);
                console.log(data.message);
            //     $.ajax({
            //         url: "http://gempe.com.br/contact_me.php",
            //         type: "POST",
            //         data: data,
            //         cache: false,
            //         success: function() {
            //             // Enable button & show success message
            //             console.log("Nos falamos em breve!", "Em até 24hs nosso time entrará em contato!", "success")

            //             //clear all fields
            //             $('#form')[0].reset();
            //         },
            //         error: function() {
            //             // Fail message
            //             console.log("Oops...", "Ocorreu algum problema, por favor tente novamente!", "error");
            //             //clear all fields
            //         },
            //     });
        };

    };

})();
